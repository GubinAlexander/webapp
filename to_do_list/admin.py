from django.contrib import admin

from .models import Task, TaskList, SubTask, Tag

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass

@admin.register(TaskList)
class TaskAdmin(admin.ModelAdmin):
    pass

@admin.register(SubTask)
class SubTaskAdmin(admin.ModelAdmin):
    pass

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass
