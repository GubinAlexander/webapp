# Generated by Django 2.0.6 on 2018-08-26 18:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('to_do_list', '0002_auto_20180826_1832'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='name')),
                ('created_user', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_lists', to=settings.AUTH_USER_MODEL)),
                ('users', models.ManyToManyField(blank=True, default=None, related_name='all_lists', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'task lists',
                'verbose_name': 'task list',
            },
        ),
    ]
