# Generated by Django 2.0.6 on 2018-08-26 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('to_do_list', '0006_task_task_list'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='priority',
            field=models.CharField(choices=[('0', 'High'), ('1', 'Medium'), ('2', 'Low'), ('-1', 'None')], default=-1, max_length=255, verbose_name='priority'),
        ),
    ]
