from dateutil.relativedelta import *

from .models import Task
from django.utils import timezone


def check_overdue():     # проверка на deadline, если deadline < currentdate: status просрочен
    now = timezone.now()
    Task.objects.filter(deadline__gt=now, status='0').update(status='P')
    Task.objects.filter(deadline__lte=now, status='P').update(status='O')


def checkable(foo):     # декоратор который дергает вернюю функ
    def wrapper(*args, **kwargs):
        check_overdue()
        return foo(*args, **kwargs)
    return wrapper


def complete_task(task, user):  # Может быть год месяц, неделя. Если неделя, то можно ыбирать еще и день. Какой следующий день недели. Если был deadline вторник, а выполняется по вторникам, то
    check_overdue()
    if task.period_val and task.period_val != 'N' and task.period_val != '':
        for st in task.subtask_set.all():
            st.status = 'P'
        if task.period_val == 'D':
            task.deadline += relativedelta(days=+task.period_count)
        elif task.period_val == 'W':
            if task.repeat_days and len(task.repeat_days):
                last_weekday = task.deadline.weekday()
                days = [MO, TU, WE, TH, FR, SA, SU]
                new_week = True
                for day in task.repeat_days:
                    print('day:', day, last_weekday)
                    if int(day) - 1 > last_weekday:
                        new_week = False
                        task.deadline += relativedelta(weekday=days[int(day)-1])
                        break
                if new_week:
                    first_day_number = int(task.repeat_days[0]) - 1
                    task.deadline += relativedelta(weeks=task.period_count - 1, weekday=days[first_day_number])
            else:
                task.deadline += relativedelta(weeks=+task.period_count)
        elif task.period_val == 'M':
            task.deadline += relativedelta(months=+task.period_count)
        elif task.period_val == 'Y':
            task.deadline += relativedelta(years=+task.period_count)
    else:
        for st in task.subtask_set.all():
            st.status = 'C'
        task.completed_user = user
        task.status = 'C'
    return task


def validate_data(priority, period, days, count):
    if priority not in map(str, range(-1, 3)):
        return False
    if period not in ['D', 'W', 'M', 'Y', 'N']:
        return False
    for day in days:
        try:
            day = int(day)
        except (ValueError, TypeError):
            return False
        if day < 1 or day > 7:
            return False
    if count < 1:
        return False
    return True
