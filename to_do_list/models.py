from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from multiselectfield import MultiSelectField
from .consts import STATUS_FOR_SUBTASK, STATUS, PERIODS, DAYS, PRIORITIES


class Tag(models.Model):
    users = models.ManyToManyField(User, default=None, blank=True)
    name = models.CharField(_('name'), max_length=20)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('tags')

    def __str__(self):
        return '%s' % self.name

class TaskList(models.Model):
    name = models.CharField(_('name'), max_length=20)
    is_private = models.BooleanField(_('is_private'), default=True)
    users = models.ManyToManyField(User, related_name=('all_lists'), default=None, blank=True)
    created_user = models.ForeignKey(User, related_name=('created_lists'), on_delete=models.CASCADE, default=None,
                                     null=True, blank=True)

    class Meta:
        verbose_name = _('task list')
        verbose_name_plural = _('task lists')

    def __str__(self):
        return '%s' % self.name

class Task(models.Model):
    title = models.CharField(_('title'), max_length=250)
    description = models.CharField(_('description'), max_length=250)
    created = models.DateTimeField(_('created'), auto_now=True, null=False, blank=True)
    deadline = models.DateTimeField(_('deadline'), blank=True, default=None, null=True)
    created_user = models.ForeignKey(User, related_name=('created_tasks'), on_delete=models.CASCADE, default=None,  # если удаляем юзера, то удаляются се таски, которые закреплены за юзером
                                     null=True, blank=True)
    completed_user = models.ForeignKey(User, related_name=('completed_tasks'), on_delete=models.CASCADE, default=None,
                                       null=True, blank=True)

    tags = models.ManyToManyField(Tag, blank=True)

    task_list = models.ForeignKey(TaskList, on_delete=models.CASCADE, default=None, blank=True)
    priority = models.CharField(_('priority'), choices=PRIORITIES, max_length=255, default=-1)
    status = models.CharField(_('status'), choices=STATUS, default='P', max_length=1)
    repeat_days = MultiSelectField(_('repeat days'),max_length=10, max_choices=7, choices=DAYS, null=True, blank=True)
    period_count = models.IntegerField(_('period count'), default=0, blank=True)
    period_val = models.CharField(max_length=1, choices=PERIODS, blank=True, default='N')

    class Meta:
        verbose_name = _('task')
        verbose_name_plural = _('tasks')
        ordering = ('deadline',)

    def save(self, *args, **kwargs): # чтобы в create было реальное время указано
        if not self.id:
            self.created = timezone.now()
        return super(Task, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

class SubTask(models.Model):
    title = models.CharField(_('title'),max_length=100)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=False, null=False)
    status = models.CharField(max_length=1, default='P', choices=STATUS_FOR_SUBTASK)

    class Meta:
        verbose_name = _('subtask')
        verbose_name_plural = _('subtasks')

    def __str__(self):
        return self.title