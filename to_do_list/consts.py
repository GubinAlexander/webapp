from django.utils.translation import ugettext_lazy as _

PERIODS = (
        ('D', _('Day')),
        ('W', _('Week')),
        ('M', _('Month')),
        ('Y', _('Year')),
        ('N', _('None'))
)

STATUS_FOR_SUBTASK = (
        ('P', _('Pending')),
        ('C', _('Completed')),
)


PRIORITIES = (
        ('0', _('High')),
        ('1', _('Medium')),
        ('2', _('Low')),
        ('-1', _('None'))
)

STATUS = (
        ('P', _('Pending')),
        ('O', _('Overdue')),
        ('C', _('Completed')),
        ('T', _('Trash')),
)

DAYS = (
        (1, _('Monday')),
        (2, _('Tuesday')),
        (3, _('Wednesday')),
        (4, _('Thursday')),
        (5, _('Friday')),
        (6, _('Saturday')),
        (7, _('Sunday')),
)
