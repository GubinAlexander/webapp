from .models import TaskList, Tag
'''
пробрасывать аргументы в base.html, нужна инфа из базы в панель с 
task_lists:
tags': ,
user': 
request_path
'''

def add_variable_to_context(request):
    tasklists = None
    tags = None
    if not request.user.is_anonymous:
        tasklists = TaskList.objects.filter(users__in=[request.user])
        tags = Tag.objects.filter(users__in=[request.user])
    return {
        'task_lists': tasklists,
        'tags': tags,
        'user': request.user,
        'request_path': request.path,
    }