from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^account/', include('account.urls')),
    url(r'^todolist/', include('to_do_list.urls'), name='todolist'),
]
